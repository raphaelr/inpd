PREFIX = /usr/local

PKG_CONFIG = pkg-config

INCS = `$(PKG_CONFIG) --cflags sdl2 SDL2_ttf`
LIBS = `$(PKG_CONFIG) --libs sdl2 SDL2_ttf`

YCFLAGS = $(INCS)
XCFLAGS = $(YCFLAGS) $(CFLAGS)
XLDFLAGS = $(LIBS) $(LDFLAGS)
