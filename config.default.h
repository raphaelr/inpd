// inpd - SDL2 joystick input display
// See LICENSE file for copyright and license details.
static const int joynum = 0;
static const int deadzone = 10000; // Out of 32767

static Btn btns[] = {
    // Use a tool like https://gamepad-tester.com/ to determine the axis/button IDs
    // { Axis, Direction, Text }
    { AXIS(0), -1, "<" },
    { AXIS(1), -1, "^" },
    { AXIS(1), 1, "V" },
    { AXIS(0), 1, ">" },

    // { BUTTON, BtnNumber, Text }
    { BUTTON, 2, "B" },
    { BUTTON, 1, "A" },
    { BUTTON, 6, "L" },
    { BUTTON, 7, "R" },
    { BUTTON, 0, "Select" },
    { BUTTON, 9, "Start" },
    { BUTTON, 3, "Turbo_A" },
};

// R, G, B values from 0-255
#define BG 0, 0, 0
#define FG 255, 255, 255
static const char *fontname = "PixelOperatorHB8.ttf";
static const int fontsize = 16;
static const int igap = 10; // gaps between buttons
static const int ogap = 2;  // gaps around the window border
