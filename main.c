// inpd - SDL2 joystick input display
// See LICENSE file for copyright and license details.
#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>
#include <SDL_ttf.h>

#define BUTTON (0)
#define AXIS(n) (2000+n)
#define HATN(n) (1000+n)
#define HAT HATN(0)
#define UP SDL_HAT_UP
#define RIGHT SDL_HAT_RIGHT
#define DOWN SDL_HAT_DOWN
#define LEFT SDL_HAT_LEFT
typedef struct {
    int axis;
    int key;
    const char *text;
    int x;
    int down;
    SDL_Surface *surf;
} Btn;

static void init(void);
static void show(const Btn *btn);
static void fail(const char *p, const char *q);

static SDL_Window *win;
static SDL_Surface *surf;
static Uint32 bg, fg;
static SDL_Joystick *joy;

#include "config.h"
#define N (sizeof btns/sizeof btns[0])
#define B (btns[i])

int main()
{
    init();
    SDL_FillRect(surf, NULL, bg);
    SDL_UpdateWindowSurface(win);

    SDL_Event e;
    while (SDL_WaitEvent(&e)) {
        switch (e.type) {
            case SDL_JOYAXISMOTION:
                for (int i=0; i<N; i++) {
                    if (B.axis == AXIS(e.jaxis.axis)) {
                        B.down = B.key == (e.jaxis.value < -deadzone ? -1 : e.jaxis.value > deadzone ? 1 : 0);
                    }
                    show(&B);
                }
                break;
            case SDL_JOYHATMOTION:
                for (int i=0; i<N; i++) {
                    if (B.axis == HATN(e.jhat.hat)) {
                        B.down = B.key & e.jhat.value;
                    }
                    show(&B);
                }
                break;
            case SDL_JOYBUTTONUP:
            case SDL_JOYBUTTONDOWN:
                for (int i=0; i<N; i++) {
                    if (B.axis == BUTTON && B.key == e.jbutton.button) {
                        B.down = e.jbutton.state;
                    }
                    show(&B);
                }
                break;
            case SDL_JOYDEVICEADDED:
                if (e.jdevice.which == joynum) {
                    if (joy) SDL_JoystickClose(joy);
                    joy = SDL_JoystickOpen(joynum);
                }
                break;
            case SDL_QUIT:
                return 0;
        }
    }
    return 0;
}

void show(const Btn *btn)
{
    SDL_Rect rect = { btn->x, ogap, btn->surf->w, btn->surf->h };
    if (btn->down) {
        SDL_BlitSurface(btn->surf, NULL, surf, &rect); 
    } else {
        SDL_FillRect(surf, &rect, bg);
    }
    SDL_UpdateWindowSurface(win);
}

void init(void)
{
    SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS, "1");
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0) fail("SDL_Init", SDL_GetError());
    if (joynum >= SDL_NumJoysticks()) fail("Joystick not connected", "");
    joy = SDL_JoystickOpen(joynum);
    if (!joy) fail("SDL_JoystickOpen", SDL_GetError());
    if (TTF_Init() < 0) fail("TTF_Init", TTF_GetError());
    TTF_Font *font = TTF_OpenFont(fontname, fontsize);
    if (!font) fail("TTF_OpenFont", TTF_GetError());

    int w = ogap, h = 0;
    SDL_Color xfg = {FG};
    for (int i=0; i<N; i++) {
        SDL_Surface *x = TTF_RenderText_Solid(font, B.text, xfg);
        if (!x) fail("TTF_RenderText_Solid", TTF_GetError());
        if (x->h > h) h = x->h;
        if (i) w += igap;
        B.x = w;
        w += x->w;
        B.surf = x;
    }
    w += ogap;
    h += 2*ogap;
    TTF_CloseFont(font);

    win = SDL_CreateWindow("inpd", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        w, h, SDL_WINDOW_SHOWN | SDL_WINDOW_BORDERLESS | SDL_WINDOW_ALWAYS_ON_TOP);
    if (!win) fail("SDL_CreateWindow", SDL_GetError());
    surf = SDL_GetWindowSurface(win);
    bg = SDL_MapRGB(surf->format, BG);
    fg = SDL_MapRGB(surf->format, FG);
}

void fail(const char *p, const char *q)
{
    fprintf(stderr, "%s: %s\n", p, q);
    exit(1);
}
