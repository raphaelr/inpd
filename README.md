# inpd - SDL2 joystick input display

## Usage

Supports both analog and digital inputs, but only digital output (i.e.
the program can tell you that you pushed your analog stick up, but not
by how much).

## Building

Edit config.h to your liking. You must at least change the "btns" array.
Build and run the resulting `inpd` binary.

inpd depends on SDL2 and SDL2_ttf. On Debian-based systems, you can get
those using

    apt-get install build-essential pkg-config libsdl2-2.0-0 libsdl2-ttf-2.0-0 libsdl2-dev libsdl2-ttf-dev

Then run

    make

To build inpd.

## License

Copyright 2021 Raphael Robatsch

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## PixelOperatorHB8.ttf

The default font shipped in this repository, PixelOperatorHB8.ttf,
is in the public domain. See https://notabug.org/HarvettFox96/ttf-pixeloperator.
