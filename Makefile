# inpd - SDL2 joystick input display
# See LICENSE file for copyright and license details.
.POSIX:

include config.mk

SRC = main.c
OBJ = $(SRC:.c=.o)

all: options compile_flags.txt inpd

options:
	@echo inpd build options:
	@echo "CFLAGS  = $(XCFLAGS)"
	@echo "LDFLAGS = $(XLDFLAGS)"
	@echo "CC      = $(CC)"

config.h:
	cp config.default.h config.h

compile_flags.txt: config.mk
	@echo "$(YCFLAGS)" | tr ' ' '\n' > compile_flags.txt

.c.o:
	$(CC) $(XCFLAGS) -c $<

main.c: config.h

$(OBJ): config.h config.mk

inpd: $(OBJ)
	$(CC) -o $@ $(OBJ) $(XLDFLAGS)

clean:
	rm -f inpd compile_flags.txt $(OBJ)

.PHONY: all options clean
